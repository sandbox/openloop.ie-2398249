<?php

function timebomb_node_info() {
    return array(
            'timebomb' => array(
            'name' => t('Timebomb'),
            'base' => 'timebomb',
            'description' => t('Time limited content useful for sharing sensitive information, such as password. Content is destroyed automatically after pre-determined length of time.'),
        ),
    );
}

function timebomb_form($node, $form_state) {
    return node_content_form($node, $form_state);
}

function timebomb_token_info() {
    $types['timebomb'] = array(
        'name' => t("Timebomb Tokens"),
        'description' => t("Tokens for Timebomb content."),
    );

    $tokens['random_url'] = array(
        'name' => t("Random URL"),
        'description' => t("Randomised short URL. E.G. /zxy1w2vs"),
    );

    return array(
        'types' => $types,
        'tokens' => array(
            'timebomb' => $tokens,
        ),
    );
}

function timebomb_tokens($type, $tokens, array $data = array(), array $options = array()) {
    $replacements = array();

    if ($type == 'timebomb') {
        foreach ($tokens as $name => $original) {
            switch ($name) {
                case 'random_url':
                    $replacements[$original] = _timebomb_get_random();
                break;
            }
        }
    }

    return $replacements;
}

function timebomb_form_alter(&$form, $form_state, $form_id) {
    if ($form_id = 'timebomb_node_form') {
        $form['timebomb_expiry'][LANGUAGE_NONE][0]['#default_value']['value'] = time() + 86400;
    }
}

function timebomb_cron() {
    $query = new EntityFieldQuery();
    $results = $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'timebomb') 
        ->fieldCondition('timebomb_expiry','value',time(), '<')
        ->execute();

    if (isset($results['node'])) {
        foreach ($results['node'] as $node) {
            node_delete_multiple(array_keys($results['node']));
        }
    }
}

function _timebomb_get_random() {
    // No vowels to avoid (rude) words
    $characters = 'bcdfghjklmnpqrstvwxyz0123456789';

    $string = '';
    for ($i = 0; $i < 7; $i++) {
        $string .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $string;
}
